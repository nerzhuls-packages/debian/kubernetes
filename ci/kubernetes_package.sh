#! /bin/bash

set -e
set -u

PKG_ARCH=${1}

for PKG in kube-proxy kubelet kube-controller-manager kube-scheduler kubectl; do
    echo "Packaging ${PKG}"
    mkdir -p ${PKG}/usr/bin/
    # Fix o+x rights
    chmod o+x ./build/kubernetes/server/bin/${PKG}
    cp ./build/kubernetes/server/bin/${PKG} ${PKG}/usr/bin/
    # Replace versions
    sed -i 's/%%KUBERNETES_VERSION%%/'${KUBERNETES_PACKAGE_VERSION:1}'/g' ${PKG}/DEBIAN/control
    sed -i 's/amd64/'${PKG_ARCH}'/g' ${PKG}/DEBIAN/control
    dpkg-deb -b ${PKG}
    mv ${PKG}.deb dist/${PKG}-${PKG_ARCH}.deb
done
